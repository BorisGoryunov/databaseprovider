﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider
{
    public interface IQuery : IDisposable
    {

        void Open();

        void Open(ITransaction transaction);

        void Close();

        bool Read();

        IDbConnection Connection { get; set; }

        IDbDataParameter AddParam(string name, object value);

        IDbDataParameter AddParamAsInt(string name, int value);
        IDbDataParameter AddParamAsInt64(string name, long value);

        IDbDataParameter AddParamAsString(string name, int value);

        void SetParam(string name, object value);

        QueryField this[string name]
        {
            get;
        }

        IDbCommand Command { get; set; }

        string ProcedureName { get; set; }
        string CommandText { get; set; }
        void ExecuteNonQuery(ITransaction transaction);

        void ExecuteNonQuery();

        void ExecuteScalar(ITransaction transaction);

        void ExecuteScalar();
    }
}
