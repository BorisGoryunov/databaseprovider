﻿using System.Data;

namespace DatabaseProvider
{
    class Transaction : ITransaction
    {
        public IDbTransaction DbTransaction { get; set; }

        public void Commit()
        {
            DbTransaction.Commit();
        }

        public void Dispose()
        {
            DbTransaction.Dispose();
        }

        public void Rollback()
        {
            DbTransaction.Rollback();
        }
    }


}
