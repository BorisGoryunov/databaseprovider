﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider
{
    public interface IConnection : IDisposable
    {
        string ConnectionString { get; set; }

        void Open(string connectionString);

        void Open();

        void Close();

        IQuery CreateQuery();

        ITransaction BeginTransaction();

        void Execute(Action action);

    }
}
