﻿using System;

namespace DatabaseProvider
{
    public interface IConnectionFactory : IDisposable
    {
        IConnection CreateConnection();
        //IConnection CreateConnection<T>() where T : IConnection, new();
    }
}
