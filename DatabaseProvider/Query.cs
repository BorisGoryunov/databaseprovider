﻿using System;
using System.Data;

namespace DatabaseProvider
{
    public abstract class Query : IQuery
    {
        private IDataReader _reader;
        private IDbCommand _command;
        private IDbConnection _connection;
        private QueryFieldCollection _fieldCollection;
        private string _procedureName;
        private string _commandText;
        public IDbConnection Connection
        {
            get
            {
                return _connection;
            }

            set
            {
                _connection = value;
            }
        }

        public IDbCommand Command
        {
            get
            {
                return _command;
            }

            set
            {
                _command = value;
            }
        }

        public string ProcedureName
        {
            get
            {
                return _procedureName;
            }

            set
            {
                _procedureName = value;
                _command.CommandType = CommandType.StoredProcedure;
                _commandText = value;
            }
        }

        public string CommandText
        {
            get
            {
                return _commandText;
            }

            set
            {
                _commandText = value;
                _command.CommandType = CommandType.Text;
                _command.CommandText = value;
            }
        }

        public void Close()
        {
            _reader?.Dispose();
        }

        public void Dispose()
        {
            Close();
            _command.Dispose();
        }

        public void Open()
        {
            Open(null);
        }

        public bool Read()
        {
            return _reader.Read();
        }

        private IDbDataParameter AddParam(string name)
        {
            var param = _command.CreateParameter();
            param.ParameterName = name;
            _command.Parameters.Add(param);
            return param;
        }

        public IDbDataParameter AddParam(string name, object value)
        {
            var param = AddParam(name);
            param.Value = value;
            return param;
        }

        public IDbDataParameter AddParamAsInt(string name, int value)
        {
            var param = AddParam(name);
            param.DbType = DbType.Int32;
            param.Value = value;
            return param;
        }

        public IDbDataParameter AddParamAsInt64(string name, long value)
        {
            var param = AddParam(name);
            param.DbType = DbType.Int64;
            param.Value = value;
            return param;
        }

        public IDbDataParameter AddParamAsString(string name, int value)
        {
            var param = AddParam(name);
            param.DbType = DbType.String;
            param.Value = value;
            return param;
        }

        public void SetParam(string name, object value)
        {
            _command.Parameters[name] = value;
        }

        public void Open(ITransaction transaction)
        {
            Close();
            _command.Transaction = transaction?.DbTransaction;
            _command.CommandText = _commandText;
            _reader = _command.ExecuteReader();
            _fieldCollection = new QueryFieldCollection(_reader);
        }

        public void ExecuteNonQuery(ITransaction transaction)
        {
            _command.CommandText = _commandText;
            _command.Transaction = transaction?.DbTransaction;
            _command.ExecuteNonQuery();
        }

        public void ExecuteNonQuery()
        {
            ExecuteNonQuery(null);
        }

        public void ExecuteScalar(ITransaction transaction)
        {
            _command.Transaction = transaction?.DbTransaction;
            _command.ExecuteScalar();
        }

        public void ExecuteScalar()
        {
            ExecuteScalar();
        }


        public QueryField this[string name]
        {
            get
            {
                return _fieldCollection[name];
            }
        }
    }
}
