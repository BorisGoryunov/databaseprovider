﻿using System;
using System.Data;

namespace DatabaseProvider
{
    public interface ITransaction : IDisposable
    {
        IDbTransaction DbTransaction { get; set; }

        void Commit();

        void Rollback();
    }
}
