﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider
{
    public abstract class ConnectionFactory<TConnection> : IConnectionFactory where TConnection : IConnection, new()
    {
        private readonly IList<IConnection> _connections = new List<IConnection>();


        private void Add(IConnection item)
        {
            _connections.Add(item);
        }

        public IConnection CreateConnection()
        {
            return CreateConnection<TConnection>();
        }

        protected T CreateConnection<T>() where T : IConnection, new()
        {
            var connection = new T();
            Add(connection);
            return connection;
        }

        public void Dispose()
        {
            foreach (var connection in _connections)
            {
                connection.Dispose();
            }
        }
    }
}
