﻿using System;
using System.Data;

namespace DatabaseProvider
{
    public abstract class Connection<TConnection> : IConnection
        where TConnection : IDbConnection, new()
    {
        private readonly TConnection _connection = new TConnection();

        public string ConnectionString { get; set; }

        public void Open(string connectionString)
        {
            _connection.ConnectionString = connectionString;
            _connection.Open();
        }

        public void Close()
        {
            _connection.Close();
        }

        public abstract IQuery CreateQuery();

        protected IQuery CreateQuery<T>() where T : IQuery, new()
        {
            var query = new T();
            query.Connection = _connection;
            query.Command = _connection.CreateCommand();
            return query;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public ITransaction BeginTransaction()
        {
            var transaction = new Transaction();
            transaction.DbTransaction = _connection.BeginTransaction();
            return transaction;
        }

        public void Open()
        {
            _connection.ConnectionString = ConnectionString;
            _connection.Open();
        }

        public void Execute(Action action)
        {
            Open();
            try
            {
                action();
            }
            finally
            {
                Close();
            }
        }
    }
}
