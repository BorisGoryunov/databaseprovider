﻿using System;

namespace DatabaseProvider
{
    public abstract class DatabaseContext<TFactory> : IDisposable
        where TFactory : IConnectionFactory, new()
    {
        private IConnectionFactory _factory;

        public IConnectionFactory ConnectionFactory
        {
            get
            {
                if (_factory == null)
                {
                    CreateFactory();
                }
                return _factory;
            }

            private set
            {
                _factory = value;
            }
        }

        public IConnectionFactory CreateFactory()
        {
            ConnectionFactory = new TFactory();
            return ConnectionFactory;
        }

        public void Dispose()
        {
            ConnectionFactory.Dispose();
        }
    }
}
