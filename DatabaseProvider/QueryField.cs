﻿using System;
using System.Data;
using System.IO;

namespace DatabaseProvider
{
    sealed public class QueryField
    {
        public IDataReader Reader { get; set; }
        public int Index { get; set; }

        public string Name { get; set; }

        private bool IsNull
        {
            get { return Reader.IsDBNull(Index); }
        }

        public string AsString
        {
            get
            {
                return Reader.GetString(Index);
            }
        }

        public int AsInteger
        {
            get { return (IsNull) ? 0 : Reader.GetInt32(Index); }
        }

        public long AsInt64
        {
            get { return (IsNull) ? 0 : Reader.GetInt64(Index); }
        }

        public Guid AsGuid
        {
            get { return (this.IsNull) ? Guid.Empty : Reader.GetGuid(Index); }
        }

        public DateTime AsDateTime
        {
            get { return (IsNull) ? new DateTime() : Reader.GetDateTime(Index); }
        }

        public float AsFloat
        {
            get { return (IsNull) ? 0 : Reader.GetFloat(Index); }
        }

        public double AsDouble
        {
            get { return (IsNull) ? 0 : Reader.GetDouble(Index); }
        }

        public decimal AsDecimal
        {
            get { return (IsNull) ? 0 : Reader.GetDecimal(Index); }
        }

        public bool AsBoolean
        {
            get { return (IsNull) ? false : Reader.GetBoolean(Index); }
        }

        public object Value
        {
            get { return Reader[Index]; }
        }

        public string BlobAsString
        {
            get
            {
                byte[] buffer = (byte[])Value;
                char[] chars = new char[buffer.Length / sizeof(char)];
                Buffer.BlockCopy(buffer, 0, chars, 0, buffer.Length);
                return new string(chars);
            }
        }

        public byte[] BlobAsBytes
        {
            get
            {
                return (byte[])Value;
            }
        }

        public Stream BlobAsStream
        {
            get
            {
                return new MemoryStream(BlobAsBytes);
            }
        }
    }
}
