﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider
{
    public interface IDatabaseContext
    {
        IConnectionFactory CreateFactory();
    }
}
