﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider
{
    sealed class QueryFieldCollection
    {
        private readonly IDictionary<string, QueryField> _items = new Dictionary<string, QueryField>();

        private IDataReader _reader;

        public QueryFieldCollection(IDataReader reader)
        {
            _reader = reader;
            for (int i = 0; i < _reader.FieldCount; i++)
            {
                Add(_reader.GetName(i));
            }
        }

        private void Add(string name)
        {
            var item = new QueryField();
            item.Name = name.ToUpper();
            item.Index = _items.Count;
            item.Reader = _reader;
            _items.Add(item.Name, item);
        }

        public QueryField this[string name]
        {
            get
            {
                return _items[name.ToUpper()];
            }
        }
    }
}
