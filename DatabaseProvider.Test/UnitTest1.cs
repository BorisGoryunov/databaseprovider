﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseProvider.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using (var context = new FirebirdProvider.DatabaseContext())
            {
                using (var connection = context.ConnectionFactory.CreateConnection())
                {
                    connection.Open("connectionString");
                    using (var trn = connection.BeginTransaction())
                    {
                        using (var qry = connection.CreateQuery())
                        {
                            qry.Open(trn);
                            qry.AddParam("p1", this);
                            int id = qry["id"].AsInteger;
                            trn.Commit();
                        }
                    }
                }
            }
        }
    }
}