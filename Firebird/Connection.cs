﻿using DatabaseProvider;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirebirdProvider
{
    public sealed class Connection : Connection<FirebirdSql.Data.FirebirdClient.FbConnection>
    {
        public override IQuery CreateQuery()
        {
            return CreateQuery<Query>();
        }
    }
}
