﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirebirdProvider.Helpers
{
    public static class ConnectionStringHelper
    {
        public static string SetPassword(string connectionString, string password)
        {
            FbConnectionStringBuilder csb = new FbConnectionStringBuilder(connectionString);
            csb.Password = password;
            return csb.ToString();
        }

        public static string GetPassword(string connectionString)
        {
            FbConnectionStringBuilder csb = new FbConnectionStringBuilder(connectionString);
            return csb.Password;
        }
    }
}
